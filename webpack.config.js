const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = {
  entry: "./app/index.js",  //this is my entrypoint or main file
  module: {
    rules: [
      {
        test: /\.svg$/,
        use: "svg-inline-loader",   // for svg bundle using this package
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],   // for css/scss files using this package
      },
      {
        test: /\.(js)$/,
        use: "babel-loader",    // for js files using this package
      },
    ],
  },
  output: {      // this will outputed in bundle.js file/ located in dist/bundle.js. this file will automatically create when we run build command.
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  plugins: [new HtmlWebpackPlugin()],     // this will generate index.js file
  mode: process.env.NODE_ENV === "production" ? "production" : "development",
};
